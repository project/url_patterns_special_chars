URL patterns special chars
---------------------------

Just enable the module and you'll get the following behaviour. 

When setting URL aliases (Configuration > Search and Metadata > URL Aliases), 
you can specify replacement patterns like: "url?key=[term]".

This is usually URL encoded (ie: url%3Fkey%3D%5Bterm%5D) and you'll end up 
either in a 404 page or the parametren will be ignored. 

This module will parse the URL and if it finds a potential encoded query 
string it will try to go to that URL. It will work with any encoded query, 
not only the ones set up in URL aliases (ie: path%3Ffield%3Dvalue will 
become path?field=value). 
